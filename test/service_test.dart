import 'package:brew_app/services/database.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  test('Result?', () {
    var databaseInstance = new DatabaseService();

    var passed = false;

    if (databaseInstance.findCourseByUid('LKtCeJPnDZibGYgdaD0W') != null) {
      passed = true;
    }

    expect(passed, true);
  });
}