class Constraints {
  static double containereWidth(double screenWidth) {
    if (screenWidth > 667 && screenWidth < 1200) {
      return screenWidth * 0.5;
    } else if (screenWidth > 1200 && screenWidth < 1600) {
      return screenWidth * 0.3;
    } else if (screenWidth > 1600) {
      return screenWidth * 0.2;
    } else
      return double.infinity;
  }

  static double pollWidth(double screenWidth) {
    if (screenWidth > 667 && screenWidth < 1200) {
      return screenWidth * 0.8;
    } else if (screenWidth > 1200 && screenWidth < 1600) {
      return screenWidth * 0.6;
    } else if (screenWidth > 1600) {
      return screenWidth * 0.4;
    } else
      return double.infinity;
  }
}
