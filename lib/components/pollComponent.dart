import 'package:brew_app/models/poll.dart';
import 'package:brew_app/screens/pollPage/pollPage.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PollComponentWidget extends StatefulWidget {
  PollComponentWidget({Key key, this.poll}) : super(key: key);
  final Poll poll;
  @override
  _PollComponentWidgetState createState() => _PollComponentWidgetState();
}

class _PollComponentWidgetState extends State<PollComponentWidget> {
  Poll poll;

  String postedDateTime = "";
  int agreePercent = 0;
  int disagreePercent = 0;
  bool loading = false;

  setAgreeAndDisAgreePercent() {
    int agreeCount = 0;
    int disagreeCount = 0;
    for (var vote in poll.votes) {
      if (vote.isApproved) {
        agreeCount += 1;
      } else {
        disagreeCount += 1;
      }
    }
    agreePercent = agreeCount;
    disagreePercent = disagreeCount;
  }

  @override
  void setState(fn) {
    super.setState(fn);
    setAgreeAndDisAgreePercent();
  }

  @override
  void initState() {
    super.initState();
    poll = widget.poll;
    postedDateTime =
        DateFormat('yyyy-MM-dd – kk:mm').format(poll.postedDateTime);
    setAgreeAndDisAgreePercent();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PollPage(
                poll: poll,
              ),
            ),
          );
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * 2.32,
          constraints: BoxConstraints(
            maxWidth: 720,
            maxHeight: 232,
          ),
          decoration: BoxDecoration(
            color: Color(0xFFEFEFEF),
            borderRadius: BorderRadius.circular(16),
            border: Border.all(
              color: Color(0xFF646464),
            ),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: 56,
                decoration: BoxDecoration(
                  color: Color(0xFFCDCDCD),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(0),
                    bottomRight: Radius.circular(0),
                    topLeft: Radius.circular(16),
                    topRight: Radius.circular(16),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(4, 4, 4, 4),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(8, 0, 0, 0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(60),
                              child: Image.network(
                                'https://picsum.photos/seed/573/600',
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(8, 0, 0, 0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  poll.owner.firstName +
                                      " " +
                                      poll.owner.secondName,
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                  ),
                                ),
                                Text(
                                  postedDateTime,
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.normal,
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                      IconButton(
                        onPressed: () {
                          print('IconButton pressed ...');
                        },
                        icon: Icon(
                          Icons.keyboard_control,
                          color: Colors.black,
                          size: 30,
                        ),
                        iconSize: 30,
                      )
                    ],
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(16),
                    bottomRight: Radius.circular(16),
                    topLeft: Radius.circular(0),
                    topRight: Radius.circular(0),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: Color(0xFFEEEEEE),
                        ),
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
                          child: Text(
                            poll.title,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontFamily: 'Poppins', color: Colors.black),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 24,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(0),
                          ),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                flex: agreePercent,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Color(0xFF20D527),
                                    borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(20),
                                      bottomRight: Radius.circular(0),
                                      topLeft: Radius.circular(20),
                                      topRight: Radius.circular(0),
                                    ),
                                  ),
                                  child: Text(
                                    '$agreePercent',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontFamily: 'Poppins',
                                        color: Colors.white),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: disagreePercent,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Color(0xFFE42222),
                                    borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(0),
                                      bottomRight: Radius.circular(20),
                                      topLeft: Radius.circular(0),
                                      topRight: Radius.circular(20),
                                    ),
                                  ),
                                  child: Text(
                                    '$disagreePercent',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontFamily: 'Poppins',
                                        color: Colors.white),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 34,
                            decoration: BoxDecoration(
                              color: Color(0xAAAAAAAA),
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                IconButton(
                                  onPressed: () {
                                    print('IconButton pressed ...');
                                  },
                                  icon: Icon(
                                    Icons.accessibility,
                                    color: Color(0xFF303030),
                                    size: 22,
                                  ),
                                  iconSize: 22,
                                ),
                                Expanded(
                                  flex: 10,
                                  child: Text(
                                    poll.teacher.name +
                                        " " +
                                        poll.teacher.surname,
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 34,
                            decoration: BoxDecoration(
                              color: Color(0xAAAAAAAA),
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                IconButton(
                                  onPressed: () {
                                    print('IconButton pressed ...');
                                  },
                                  icon: Icon(
                                    Icons.cases,
                                    color: Color(0xFF303030),
                                    size: 22,
                                  ),
                                  iconSize: 22,
                                ),
                                Expanded(
                                  flex: 10,
                                  child: Text(poll.course.name),
                                )
                              ],
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
