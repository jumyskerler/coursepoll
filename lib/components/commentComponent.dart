import 'package:brew_app/components/subCommentComponent.dart';
import 'package:brew_app/models/comment.dart';
import 'package:brew_app/models/subComment.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CommentComponent extends StatefulWidget {
  const CommentComponent({this.comment});

  final Comment comment;

  @override
  _CommentComponentState createState() => _CommentComponentState();
}

class _CommentComponentState extends State<CommentComponent> {
  final FirebaseAuth auth = FirebaseAuth.instance;
  Comment comment;
  SubComment subComments;
  String myDate = "";
  @override
  void initState() {
    super.initState();
    comment = widget.comment;
    myDate = DateFormat("HH:mm dd.MM.yyyy").format(comment.dateTime);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      child: Column(
        children: [
          Card(
            elevation: 0,
            child: ListTile(
              onTap: () {},
              leading: Icon(Icons.account_circle),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "${comment.user.firstName} at $myDate",
                    style: TextStyle(fontSize: 12),
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  Text(comment.commentText),
                ],
              ),
              trailing: IconButton(
                icon: Icon(Icons.reply_outlined),
                onPressed: () {
                  setState(() {
                    
                  });
                },
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 5.0),
            child: Container(
              child: Column(
                  children: comment.subComments
                      .map((subComment) =>
                          SubCommentComponent(subComment: subComment))
                      .toList()),
            ),
          ),
          Divider()
        ],
      ),
    );

    //      Column(children: [
    //   Row(
    //     children: [
    //       Text(comment.user.firstName + " " + comment.user.secondName)
    //     ],
    //   ),
    //   Row(
    //     children: [Text(comment.commentText)],
    //   )
    // ]));
  }
}
