import 'package:brew_app/models/comment.dart';
import 'package:brew_app/models/subComment.dart';
import 'package:flutter/material.dart';

class SubCommentComponent extends StatelessWidget {
  const SubCommentComponent({
    Key key,
    @required this.subComment,
  }) : super(key: key);

  final SubComment subComment;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      child: ListTile(
        leading: Icon(Icons.account_circle),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "${subComment.user.firstName} at ${subComment.dateTime}",
              style: TextStyle(fontSize: 12),
            ),
            SizedBox(
              height: 7,
            ),
            Text(subComment.commentText),
          ],
        ),
      ),
    );
  }
}
