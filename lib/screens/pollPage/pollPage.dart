import 'package:brew_app/components/commentComponent.dart';
import 'package:brew_app/models/poll.dart';
import 'package:brew_app/services/database.dart';
import 'package:brew_app/shared/constraints.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class PollPage extends StatefulWidget {
  final Poll poll;
  PollPage({this.poll});

  @override
  _PollPageState createState() => _PollPageState();
}

class _PollPageState extends State<PollPage> {
  FirebaseAuth auth = FirebaseAuth.instance;
  DatabaseService db = DatabaseService();

  Poll poll;
  bool loading = false;
  bool loadingComments = false;
  bool isButtonDisable = false;

  String comment;

  var commentController = TextEditingController();

  addNewComment() async {
    await db.postComment(poll.uid, auth.currentUser.uid.toString(), comment);
    final scaffold = ScaffoldMessenger.of(context);
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: new Text('Successfully added new comments...'),
      action:
          SnackBarAction(label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
    ));
    setState(() {
      loadingComments = true;
    });
    var newComments = await db.getCommentsByPollUid(poll.uid);
    print("Commets lents is ${newComments.length}");
    setState(() {
      poll.comments = newComments;
      loadingComments = false;
    });
  }

  checkUserVote(Poll poll) async {
    setState(() {
      loading = true;
    });

    (await db.getVotesByPollUid(poll.uid)).forEach((element) {
      if (element.userUid == auth.currentUser.uid) {
        setState(() {
          isButtonDisable = true;
        });
      }
    });

    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    poll = widget.poll;

    checkUserVote(poll);
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          title: Text("Poll"),
          backgroundColor: Colors.black87,
        ),
        body: loading
            ? Center(child: Text('Loading'))
            : Center(
                child: Container(
                  width: Constraints.pollWidth(screenWidth),
                  child: Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 2),
                          child: Container(
                            child: Row(
                              children: [
                                Text('Course:'),
                                SizedBox(
                                  width: 2,
                                ),
                                Text(poll.course.name)
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 2),
                          child: Container(
                            child: Row(
                              children: [
                                Text('Teacher:'),
                                SizedBox(
                                  width: 2,
                                ),
                                Text(poll.teacher.name)
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 7, top: 7),
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    flex: 75,
                                    child: Container(
                                      height: 19,
                                      width: double.infinity,
                                      color: Colors.greenAccent[400],
                                      child: Center(child: Text('75%')),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 25,
                                    child: Container(
                                      child: Center(child: Text('25%')),
                                      height: 19,
                                      color: Colors.red,
                                    ),
                                  ),
                                ]),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                ElevatedButton(
                                  onPressed: isButtonDisable
                                      ? null
                                      : () async {
                                          await db.postVote(poll.uid, true);
                                          setState(() {
                                            isButtonDisable = true;
                                          });
                                        },
                                  child: Text(
                                    'Agree',
                                  ),
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.greenAccent[400],
                                  ),
                                ),
                                ElevatedButton(
                                  onPressed: isButtonDisable
                                      ? null
                                      : () async {
                                          await db.postVote(poll.uid, false);
                                          setState(() {
                                            isButtonDisable = true;
                                          });
                                        },
                                  child: Text('Disagree'),
                                  style: ElevatedButton.styleFrom(
                                      primary: Colors.red),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 2),
                          child: Container(
                            child: Row(
                              children: [
                                Text('Description:'),
                                SizedBox(
                                  width: 2,
                                ),
                                Text(poll.description)
                              ],
                            ),
                          ),
                        ),
                        Divider(),
                        Expanded(
                          child: ListView(
                            padding: const EdgeInsets.all(5.0),
                            scrollDirection: Axis.vertical,
                            children: loadingComments
                                ? [
                                    Center(
                                      child: Text("Loading comments"),
                                    )
                                  ]
                                : poll.comments
                                    .map((comment) =>
                                        CommentComponent(comment: comment))
                                    .toList(),
                          ),
                        ),
                        Container(
                            margin: EdgeInsets.zero,
                            child: ListTile(
                              title: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: TextField(
                                      controller: commentController,
                                      onChanged: (value) {
                                        comment = value;
                                      },
                                      onSubmitted: (value) async {
                                        await addNewComment();
                                        commentController.clear();
                                      },
                                    ),
                                  ),
                                ],
                              ),
                              trailing: IconButton(
                                  icon: Icon(Icons.chevron_right),
                                  color: Colors.black26,
                                  onPressed: () async {
                                    await addNewComment();
                                    commentController.clear();
                                  }),
                            )),
                      ],
                    ),
                  ),
                ),
              ));
  }
}
