import 'package:brew_app/services/auth.dart';
import 'package:flutter/material.dart';

// Placeholder Widget
class PlaceholderWidget extends StatelessWidget {
  final AuthService _authService = AuthService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Search'),
        elevation: 0,
        backgroundColor: Colors.black87,
        actions: [
          FlatButton.icon(
            textColor: Colors.white,
            onPressed: () {},
            icon: Icon(Icons.search),
            label: Text("Search"),
          ),
        ],
      ),
      body: Container(child: Center(child: Text('Test'))),
    );
  }
}
