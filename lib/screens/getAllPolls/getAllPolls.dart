import 'package:brew_app/components/pollComponent.dart';
import 'package:brew_app/models/poll.dart';
import 'package:brew_app/services/auth.dart';
import 'package:brew_app/services/database.dart';
import 'package:brew_app/shared/constraints.dart';
import 'package:flutter/material.dart';

class GetAllPolls extends StatefulWidget {
  @override
  _GetAllPollsState createState() => _GetAllPollsState();
}

class _GetAllPollsState extends State<GetAllPolls> {
  final AuthService _authService = AuthService();
  final DatabaseService db = DatabaseService();

  List<Poll> polls = [];
  bool loadingPolls = false;

  getPoll() async {
    if (this.mounted) {
      setState(() {
        loadingPolls = true;
      });
    }

    polls = await db.getFirstBatchOfPolls();

    if (this.mounted) {
      setState(() {
        loadingPolls = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getPoll();
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('Polls'),
          backgroundColor: Colors.black87,
          actions: [],
        ),
        body: loadingPolls == true
            ? Center(
                child: Text('Loading...'),
              )
            : Center(
                child: Container(
                  width: Constraints.pollWidth(screenWidth),
                  child: ListView.builder(
                      itemCount: polls.length,
                      itemBuilder: (BuildContext context, int index) {
                        return PollComponentWidget(poll: polls[index]);
                      }),
                ),
              ));
  }
}
