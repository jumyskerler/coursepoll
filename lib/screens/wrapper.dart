import 'package:brew_app/models/myUser.dart';
import 'package:brew_app/screens/authentication/authentication.dart';
import 'package:brew_app/screens/home/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Wrappper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<MyUser>(context);

    if (user == null) {
      return Authentication();
    }

    return Home();
  }
}
