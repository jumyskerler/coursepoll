import 'package:brew_app/components/pollComponent.dart';
import 'package:brew_app/models/myUser.dart';
import 'package:brew_app/models/poll.dart';
import 'package:brew_app/models/vote.dart';
import 'package:brew_app/services/auth.dart';
import 'package:brew_app/services/database.dart';
import 'package:brew_app/shared/constraints.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final AuthService _authService = AuthService();
  final FirebaseAuth auth = FirebaseAuth.instance;
  final DatabaseService db = DatabaseService();
  final ImageErrorListener onBackgroundImageError = null;

  MyUser user = MyUser();
  List<Poll> userVotedPollsHistory = [];
  List<Vote> userVotes = [];
  int agree = 0;
  int disagree = 0;
  bool loading = false;
  bool loadingHistory = false;

  getUser(String userUid) async {
    setState(() {
      loading = true;
    });

    user = await db.getUserDetailByUserUid(userUid);

    setState(() {
      loading = false;
    });
  }

  getPoll(String userUid) async {
    setState(() {
      loadingHistory = true;
    });

    userVotes = await db.getVotesByUserUid(userUid);

    for (var vote in userVotes) {
      userVotedPollsHistory.add(await db.findPollByUid(vote.pollUid));
    }

    setState(() {
      loadingHistory = false;
    });
  }

  setAgreeAndDisagree() {
    var agreeCount = 0;
    var disagreeCount = 0;
    for (var vote in userVotes) {
      if (vote.isApproved == true) {
        agreeCount++;
      } else {
        disagreeCount++;
      }
    }
    agree = agreeCount;
    disagree = disagreeCount;
  }

  @override
  void setState(fn) {
    super.setState(fn);
    setAgreeAndDisagree();
  }

  @override
  void initState() {
    super.initState();
    getUser(auth.currentUser.uid);
    getPoll(auth.currentUser.uid);
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: loading == true
          ? AppBar(
              title: Text('Loading..'),
              elevation: 0,
              backgroundColor: Colors.white)
          : AppBar(
              title: Text(user.email),
              elevation: 0,
              backgroundColor: Colors.black87,
              actions: [
                FlatButton.icon(
                    textColor: Colors.white,
                    onPressed: () async {
                      await _authService.signOut();
                    },
                    icon: Icon(Icons.logout),
                    label: Text('logout')),
              ],
            ),
      body: loading == true
          ? Center(child: Text('Loading..'))
          : Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [Colors.black87, Colors.white70])),
                      child: Container(
                        width: double.infinity,
                        height: 350.0,
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              CircleAvatar(
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(200),
                                  child: Image.network(
                                    'https://picsum.photos/seed/573/600',
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                backgroundColor: Colors.blue.shade50,
                                onBackgroundImageError: onBackgroundImageError,
                                radius: 50.0,
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                user.firstName + " " + user.secondName,
                                style: TextStyle(
                                  fontSize: 22.0,
                                  color: Colors.white,
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Container(
                                width: Constraints.pollWidth(screenWidth),
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(16),
                                  ),
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 8.0, vertical: 8.0),
                                  clipBehavior: Clip.antiAlias,
                                  elevation: 16,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0, vertical: 22.0),
                                    child: Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Column(
                                            children: <Widget>[
                                              Text(
                                                "Agree",
                                                style: TextStyle(
                                                  color: Colors
                                                      .greenAccent.shade700,
                                                  fontSize: 22.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 5.0,
                                              ),
                                              loadingHistory
                                                  ? Text(" ")
                                                  : Text(
                                                      agree.toString(),
                                                      style: TextStyle(
                                                        fontSize: 20.0,
                                                        color: Colors
                                                            .greenAccent
                                                            .shade700,
                                                      ),
                                                    )
                                            ],
                                          ),
                                        ),
                                        Expanded(
                                          child: Column(
                                            children: <Widget>[
                                              Text(
                                                "Overall",
                                                style: TextStyle(
                                                  color: Colors.blue.shade700,
                                                  fontSize: 22.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 5.0,
                                              ),
                                              loadingHistory
                                                  ? Text(" Loading ")
                                                  : Text(
                                                      userVotes.length
                                                          .toString(),
                                                      style: TextStyle(
                                                        fontSize: 20.0,
                                                        color: Colors
                                                            .blue.shade700,
                                                      ),
                                                    )
                                            ],
                                          ),
                                        ),
                                        Expanded(
                                          child: Column(
                                            children: <Widget>[
                                              Text(
                                                "Disagree",
                                                style: TextStyle(
                                                  color: Colors.red.shade700,
                                                  fontSize: 22.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 5.0,
                                              ),
                                              loadingHistory
                                                  ? Text(" ")
                                                  : Text(
                                                      disagree.toString(),
                                                      style: TextStyle(
                                                        fontSize: 20.0,
                                                        color:
                                                            Colors.red.shade700,
                                                      ),
                                                    )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )),
                ),

                // MARK - Doesn't work ListView item's Widget !
                //
                Expanded(
                  child: Container(
                    // height: MediaQuery.of(context).size.height,
                    child: loadingHistory
                        ? Center(
                            child: Text("loading history..."),
                          )
                        : Center(
                            child: Container(
                              width: Constraints.pollWidth(screenWidth),
                              child: ListView(
                                scrollDirection: Axis.vertical,
                                children: userVotedPollsHistory
                                    .map((e) => PollComponentWidget(poll: e))
                                    .toList(),
                              ),
                            ),
                          ),
                  ),
                )
                // Column(
                //   children: loading
                //       ? [
                //           Center(
                //             child: Text('Loading...'),
                //           )
                //         ]
                //       : userVotedPollsHistory
                //           .map((e) => PollComponent(poll: e))
                //           .toList(),
                // )
              ],
            ),
    );
  }
}
