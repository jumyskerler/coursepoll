import 'package:brew_app/models/course.dart';
import 'package:brew_app/models/teacher.dart';
import 'package:brew_app/services/auth.dart';
import 'package:brew_app/services/database.dart';
import 'package:brew_app/shared/constraints.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

class AddPoll extends StatefulWidget {
  @override
  _AddPollState createState() => _AddPollState();
}

class _AddPollState extends State<AddPoll> {
  final AuthService _authService = AuthService();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final DatabaseService databaseService = DatabaseService();

  String teacherChoose;
  String courseChoose;
  String description;
  String title;
  String courseUid;
  String teacherUid;

  List<Teacher> teachers = [];
  List<Course> courses = [];
  bool loading = false;

  getTeachersAndTeachers() async {
    setState(() {
      loading = true;
    });
    teachers.addAll(await databaseService.getTeachers());
    courses.addAll(await databaseService.getCourses());
    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getTeachersAndTeachers();
  }

  // TODO: Отображать выбранный option из DropDownButton

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          title: Text('New Poll'),
          elevation: 0,
          backgroundColor: Colors.black87,
          actions: [],
        ),
        body: loading == true
            ? Center(child: Text('Loading..'))
            : Center(
                child: Container(
                  width: Constraints.pollWidth(screenWidth),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListView(shrinkWrap: screenWidth > 720, children: [
                      Card(
                        child: Column(
                          children: [
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              child: TextField(
                                style: TextStyle(
                                    fontSize: 32, fontWeight: FontWeight.w600),
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(14),
                                    border: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    errorBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    hintText: 'Enter title here',
                                    hintStyle: TextStyle(
                                        fontSize: 32,
                                        fontWeight: FontWeight.normal)),
                                onChanged: (value) {
                                  setState(() {
                                    title = value;
                                  });
                                },
                              ),
                            ),
                            Container(
                              height: 200,
                              child: TextField(
                                keyboardType: TextInputType.multiline,
                                maxLines: null,
                                decoration: InputDecoration(
                                  hintText: 'Enter description',
                                  contentPadding:
                                      EdgeInsets.only(left: 14, right: 14),
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  errorBorder: InputBorder.none,
                                  disabledBorder: InputBorder.none,
                                ),
                                onChanged: (value) {
                                  setState(() {
                                    description = value;
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Card(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton(
                                    hint: Text('Choose teacher'),
                                    value: teacherChoose,
                                    onChanged: (newValue) {
                                      setState(() {
                                        teacherChoose = newValue;
                                      });
                                    },
                                    items: teachers.map((mapValue) {
                                      teacherUid = mapValue.uid;
                                      return DropdownMenuItem(
                                        // dropDown выбирают ИМЕННО это значение!
                                        value: mapValue.name,
                                        child: Text(mapValue.name),
                                      );
                                    }).toList()),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton(
                                    hint: Text('Choose course'),
                                    value: courseChoose,
                                    onChanged: (newValue) {
                                      setState(() {
                                        courseChoose = newValue;
                                      });
                                    },
                                    items: courses.map((mapValue) {
                                      courseUid = mapValue.uid;
                                      return DropdownMenuItem(
                                          value: mapValue.name,
                                          child: Text(mapValue.name));
                                    }).toList()),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ElevatedButton(
                            onPressed: () async {
                              await databaseService.postPoll(
                                  title,
                                  _firebaseAuth.currentUser.uid,
                                  description,
                                  courseUid,
                                  teacherUid);
                              final scaffold = ScaffoldMessenger.of(context);
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(
                                content:
                                    new Text('Successfully added new poll'),
                                action: SnackBarAction(
                                    label: 'OK',
                                    onPressed: scaffold.hideCurrentSnackBar),
                              ));
                            },
                            child: Text('Add Poll')),
                      )
                    ]),
                  ),
                ),
              ));
  }
}
