import 'package:brew_app/screens/authentication/sign_in.dart';
import 'package:brew_app/screens/authentication/verifyEmail.dart';
import 'package:brew_app/services/auth.dart';
import 'package:brew_app/shared/constants.dart';
import 'package:brew_app/shared/constraints.dart';
import 'package:brew_app/shared/loading_screen.dart';
import 'package:flutter/material.dart';

class Regiter extends StatefulWidget {
  final Function toggleView;
  Regiter({this.toggleView});
  @override
  _RegiterState createState() => _RegiterState();
}

class _RegiterState extends State<Regiter> {
  final AuthService _authService = AuthService();
  final _formKey = GlobalKey<FormState>();

  String email;
  String password;
  String confirmPassword;
  String firstName;
  String secondName;

  String error = '';
  bool loading = false;
  bool showPassword = true;

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return loading
        ? Loading()
        : Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.black87,
              elevation: 0,
              title: Text('Register'),
              actions: [
                FlatButton.icon(
                    onPressed: () {
                      widget.toggleView();
                    },
                    icon: Icon(Icons.person),
                    label: Text(
                      'Sing in',
                      style: TextStyle(color: Colors.white),
                    ))
              ],
            ),
            body: Center(
              child: Container(
                width: Constraints.containereWidth(screenWidth),
                padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50),
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                          decoration:
                              textInputDecoration.copyWith(hintText: 'Email'),
                          validator: (val) =>
                              val.isEmpty ? 'Enter an email' : null,
                          onChanged: (value) {
                            setState(() {
                              return email = value;
                            });
                          }),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        decoration:
                            textInputDecoration.copyWith(hintText: 'Password'),
                        obscureText: showPassword,
                        validator: (val) => val.length < 6
                            ? 'Enter password 6+ chars long'
                            : null,
                        onChanged: (value) {
                          setState(() {
                            return password = value;
                          });
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        decoration: textInputDecoration.copyWith(
                            hintText: 'Confirm Password'),
                        obscureText: showPassword,
                        validator: (val) => val.length < 6
                            ? 'Enter password 6+ chars long'
                            : null,
                        onChanged: (value) {
                          setState(() {
                            return confirmPassword = value;
                          });
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        decoration: textInputDecoration.copyWith(
                            hintText: 'First name'),
                        validator: (val) =>
                            val.length < 1 ? 'First name is too short' : null,
                        onChanged: (value) {
                          setState(() {
                            return firstName = value;
                          });
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        decoration: textInputDecoration.copyWith(
                            hintText: 'Second name'),
                        validator: (val) =>
                            val.length < 1 ? 'Second name is too short' : null,
                        onChanged: (value) {
                          setState(() {
                            return secondName = value;
                          });
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextButton(
                        onPressed: () => setState(() {
                          showPassword = !showPassword;
                        }),
                        child: Text('Show password'),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      RaisedButton(
                        onPressed: () async {
                          if (_formKey.currentState.validate()) {
                            var message = '';

                            setState(() {
                              loading = true;
                            });

                            if (password == confirmPassword) {
                              try {
                                message = await _authService.register(
                                    email, password, firstName, secondName);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            VerifyEmailWidget()));
                              } catch (e) {
                                message = e.toString();
                              }
                            } else {
                              message = "Password are not same";
                            }
                          }

                          setState(() {
                            loading = false;
                          });
                        },
                        color: Colors.red,
                        child: Text(
                          'Register',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      SizedBox(height: 20),
                      Text(
                        error,
                        style: TextStyle(color: Colors.red, fontSize: 14),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
  }
}
