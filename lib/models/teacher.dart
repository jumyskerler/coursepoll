class Teacher {
  final String uid;
  final String name;
  final String surname;

  Teacher({this.uid, this.name, this.surname});
}