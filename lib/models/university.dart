class University {
  final String uid;
  final String name;
  final String description;

  University({this.uid, this.name, this.description});
}

