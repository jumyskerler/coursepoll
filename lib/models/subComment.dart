import 'package:brew_app/models/myUser.dart';

class SubComment {
  final String uid;
  final String mainCommentUid;
  final MyUser user;
  final String commentText;
  final DateTime dateTime;

  SubComment({this.uid, this.mainCommentUid, this.user, this.commentText, this.dateTime});
}