import 'package:brew_app/models/comment.dart';
import 'package:brew_app/models/myUser.dart';
import 'package:brew_app/models/teacher.dart';
import 'package:brew_app/models/vote.dart';
import 'course.dart';

class Poll {
  final String uid;
  final MyUser owner;
  final String title;
  final Course course;
  final Teacher teacher;
  final String description;
  final DateTime postedDateTime;
  final List<Vote> votes;
  List<Comment> comments;

  Poll(
      {this.uid,
      this.owner,
      this.title,
      this.course,
      this.teacher,
      this.description,
      this.postedDateTime,
      this.votes,
      this.comments});
}
