class Course {
  final String uid;
  final String name;

  Course({this.uid, this.name});
}