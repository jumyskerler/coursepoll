class UniversityCourse {
  final String uid;
  final String universityUid;
  final String courseUid;

  UniversityCourse({this.uid, this.courseUid, this.universityUid});
}