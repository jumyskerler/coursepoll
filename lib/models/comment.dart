import 'package:brew_app/models/myUser.dart';
import 'package:brew_app/models/subComment.dart';

class Comment {
  final String uid;
  final String pollUid;
  final MyUser user;
  final String commentText;
  final DateTime dateTime;
  final List<SubComment> subComments;

  Comment({this.uid, this.pollUid, this.user, this.commentText, this.dateTime, this.subComments});
}