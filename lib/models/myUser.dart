class MyUser {
  final String uid;
  final String firstName;
  final String secondName;
  final String email;

  MyUser({this.uid, this.firstName, this.secondName, this.email});
}
