class Vote {
  final String uid;
  final String pollUid;
  final String userUid;
  final bool isApproved;

  Vote({this.uid, this.pollUid, this.userUid, this.isApproved});
}