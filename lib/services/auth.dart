import 'package:brew_app/models/myUser.dart';
import 'package:brew_app/services/database.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  //create user obj based on FirebaseUser
  MyUser _userFromFirebase(User user) {
    return user != null && user.emailVerified ? MyUser(uid: user.uid) : null;
  }

  //auth change user stream
  Stream<MyUser> get user {
    return _auth.authStateChanges().map(_userFromFirebase);
  }

  //sign anon
  Future signInAnon() async {
    try {
      var result = await _auth.signInAnonymously();
      return _userFromFirebase(result.user);
    } catch (e) {
      return null;
    }
  }

  //sign in email and password
  Future signInWithEmailPassword(String email, String password) async {
    try {
      var result = await _auth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );

      return _userFromFirebase(result.user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //register in email and password
  Future register(String email, String password, String firstName,
      String secondName) async {
    try {
      var result = await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password
      );

      await result.user.sendEmailVerification();
      await DatabaseService(uid: result.user.uid).postUserDetail(
        '${result.user.uid}',
        firstName,
        secondName,
        email
      );

      _auth.signOut();

      return new Future<String>.value("Successfully registered, now you need to verify your account via email");
    } catch (e) {
      throw new Exception(e.toString());
    }
  }

  //sign out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      return null;
    }
  }
}
