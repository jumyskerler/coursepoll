import 'package:brew_app/models/comment.dart';
import 'package:brew_app/models/course.dart';
import 'package:brew_app/models/myUser.dart';
import 'package:brew_app/models/poll.dart';
import 'package:brew_app/models/subComment.dart';
import 'package:brew_app/models/teacher.dart';
import 'package:brew_app/models/vote.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class DatabaseService {
  // Firestore Reference
  final db = FirebaseFirestore.instance;
  final auth = FirebaseAuth.instance;

  final String uid;
  final String userDetailsString = 'user_details';
  final String pollsString = 'polls';
  final String coursesString = 'courses';
  final String teachersString = 'teachers';
  final String votesString = 'user_votes';
  final String commentsString = 'comments';
  final String subCommentsString = 'sub_comments';

  final int pollsLimit = 10;

  DatabaseService({this.uid});
}

extension UserDetailService on DatabaseService {
  Future<void> postUserDetail(String uid, firstName, secondName, email) async {
    return await db.collection(userDetailsString).doc(uid).set({
      'uid': uid,
      'firstName': firstName,
      'secondName': secondName,
      'email': email
    });
  }

  Future<List<MyUser>> getUserDetails() async {
    List<MyUser> userDetails = [];

    await db
        .collection(userDetailsString)
        .get()
        .then((QuerySnapshot querySnapshot) {
      querySnapshot.docs.forEach((document) {
        userDetails.add(MyUser(
          uid: document.id,
          firstName: document.data()['firstName'].toString(),
          secondName: document.data()['secondName'].toString(),
          email: document.data()['email'].toString(),
        ));
      });
    });

    return userDetails;
  }

  Future<MyUser> getUserDetailByUserUid(String userUid) async {
    return await db
        .collection(userDetailsString)
        .doc(userUid)
        .get()
        .then((snapshot) {
      return MyUser(
          uid: snapshot.id,
          firstName: snapshot.get('firstName'),
          secondName: snapshot.get('secondName'),
          email: snapshot.get('email'));
    });
  }
}

extension PollService on DatabaseService {
  Future<void> postPoll(
      String title, owner, description, courseUid, teacherUid) async {
    return await db.collection(pollsString).add({
      'title': title,
      'ownerUid': owner,
      'description': description,
      'courseUid': courseUid,
      'teacherUid': teacherUid,
      'postedDateTime': DateTime.now()
    });
  }

  Future<List<Poll>> getFirstBatchOfPolls() async {
    List<Poll> polls = [];
    await db
        .collection(pollsString)
        // .limit(pollsLimit)
        .orderBy('title')
        .get()
        .then((QuerySnapshot querySnapshot) async {
      await Future.wait(querySnapshot.docs.map((document) async {
        Timestamp postedDateTime = document.data()['postedDateTime'];
        polls.add(Poll(
            uid: document.id,
            owner: await this
                .getUserDetailByUserUid(document.data()['ownerUid'].toString()),
            title: document.data()['title'].toString(),
            description: document.data()['description'].toString(),
            course: await this
                .findCourseByUid(document.data()['courseUid'].toString()),
            teacher: await this
                .findTeacherByUid(document.data()['teacherUid'].toString()),
            postedDateTime: DateTime.fromMillisecondsSinceEpoch(
                postedDateTime.seconds * 1000),
            votes: await this.getVotesByPollUid(document.id),
            comments:
                await this.getCommentsByPollUid(document.data()[document.id])));
      }));
    });

    return polls;
  }

  /*

  // Принимает тайтл последнего документа
  // Оставим этот метод на потом...
  Future<List<Poll>> getNextBatchOfPolls(String title) async {
    List<Poll> polls = [];

    await db
        .collection(pollsString)
        .limit(pollsLimit)
        .orderBy('title')
        .startAfter([title])
        .get()
        .then((QuerySnapshot querySnapshot) async {
          await Future.wait(querySnapshot.docs.map((document) async {
            polls.add(Poll(
                uid: document.id,
                title: document.data()['title'].toString(),
                description: document.data()['description'].toString(),
                course: await this
                    .findCourseByUid(document.data()['courseUid'].toString()),
                teacher: await this.findTeacherByUid(
                    document.data()['teacherUid'].toString())));
          }));
        });

    return polls;
  }

   */

  Future<Poll> findPollByUid(String pollUid) async {
    return await db
        .collection(pollsString)
        .doc(pollUid)
        .get()
        .then((snapshot) async {
      Timestamp postedTimestamp = snapshot.data()['postedDateTime'];
      return Poll(
          uid: snapshot.id,
          title: snapshot.get('title'),
          owner: await this
              .getUserDetailByUserUid(snapshot.data()['ownerUid'].toString()),
          description: snapshot.get('description'),
          course: await this
              .findCourseByUid(snapshot.data()['courseUid'].toString()),
          teacher: await this
              .findTeacherByUid(snapshot.data()['teacherUid'].toString()),
          postedDateTime: DateTime.fromMillisecondsSinceEpoch(
              postedTimestamp.seconds * 1000),
          votes: await this.getVotesByPollUid(snapshot.id));
    });
  }
}

extension CourseService on DatabaseService {
  Future<void> postCourse(String name) async {
    return await db.collection(coursesString).add({'name': name});
  }

  Future<List<Course>> getCourses() async {
    List<Course> courses = [];

    await db
        .collection(coursesString)
        .get()
        .then((QuerySnapshot querySnapshot) {
      querySnapshot.docs.forEach((document) {
        courses.add(
            Course(uid: document.id, name: document.data()['name'].toString()));
      });
    });

    return courses;
  }

  Future<Course> findCourseByUid(String courseUid) async {
    return await db
        .collection(coursesString)
        .doc(courseUid)
        .get()
        .then((snapshot) {
      return Course(uid: snapshot.id, name: snapshot.get('name'));
    });
  }
}

extension TeacherService on DatabaseService {
  Future<void> postTeacher(String name, surname) async {
    return await db
        .collection(teachersString)
        .add({'name': name, 'surname': surname});
  }

  Future<List<Teacher>> getTeachers() async {
    List<Teacher> teachers = [];

    await db
        .collection(teachersString)
        .get()
        .then((QuerySnapshot querySnapshot) {
      querySnapshot.docs.forEach((document) {
        teachers.add(Teacher(
            uid: document.id,
            name: document.data()['name'].toString(),
            surname: document.data()['surname'].toString()));
      });
    });

    return teachers;
  }

  Future<Teacher> findTeacherByUid(String teacherUid) async {
    return await db
        .collection(teachersString)
        .doc(teacherUid)
        .get()
        .then((snapshot) {
      return Teacher(
          uid: snapshot.id,
          name: snapshot.get('name'),
          surname: snapshot.get('surname'));
    });
  }
}

extension VoteService on DatabaseService {
  Future<void> postVote(String pollUid, bool isApproved) async {
    return await db.collection(votesString).add({
      'pollUid': pollUid,
      'userUid': auth.currentUser.uid, // Текущий пользователь
      'isApproved': isApproved
    });
  }

  Future<List<Vote>> getVotesByPollUid(String pollUid) async {
    List<Vote> votes = [];

    await db
        .collection(votesString)
        .where('pollUid', isEqualTo: pollUid)
        .get()
        .then((QuerySnapshot querySnapshot) {
      querySnapshot.docs.forEach((document) {
        votes.add(Vote(
            uid: document.id,
            pollUid: document.data()['pollUid'],
            userUid: document.data()['userUid'],
            isApproved: document.data()['isApproved']));
      });
    });
    print("votes length: ${votes.length}");
    return votes;
  }

  Future<List<Vote>> getVotesByUserUid(String userUid) async {
    List<Vote> votes = [];
    await db
        .collection(votesString)
        .where('userUid', isEqualTo: userUid)
        .get()
        .then((QuerySnapshot querySnapshot) {
      querySnapshot.docs.forEach((document) {
        votes.add(Vote(
            uid: document.id,
            pollUid: document.data()['pollUid'],
            userUid: document.data()['userUid'],
            isApproved: document.data()['isApproved']));
      });
    });
    print("votes length: ${votes.length}");
    return votes;
  }
}

extension CommentService on DatabaseService {
  Future<void> postComment(pollUid, userUid, commentText) async {
    return await db.collection(commentsString).add({
      'pollUid': pollUid,
      'userUid': userUid,
      'commentText': commentText,
      'dateTime': DateTime.now()
    });
  }

  Future<List<Comment>> getCommentsByPollUid(String pollUid) async {
    List<Comment> comments = [];
    print("pollUid ${pollUid}");
    await db
        .collection(commentsString)
        .where('pollUid', isEqualTo: pollUid)
        .get()
        .then((QuerySnapshot querySnapshot) async {
      await Future.wait(querySnapshot.docs.map((document) async {
        Timestamp postedTimestamp = document.data()['dateTime'];
        comments.add(Comment(
            uid: document.id,
            pollUid: document.data()['pollUid'],
            commentText: document.data()['commentText'],
            dateTime: DateTime.fromMillisecondsSinceEpoch(postedTimestamp.seconds * 1000),
            user: await this.getUserDetailByUserUid(document.data()['userUid']),
            subComments: await this.getSubCommentsByMailCommentUid(document.id) ?? []
                ));
      }));
    });
    print("Nurym comment length ${comments.length}");
    return comments;
  }
}

extension SubCommentService on DatabaseService {
  Future<void> postSubComment(
      String uid, mainCommentUid, userUid, commentText, dateTime) async {
    return await db.collection(subCommentsString).add({
      'uid': uid,
      'mainCommentUid': mainCommentUid,
      'userUid': userUid,
      'commentText': commentText,
      'dateTime': dateTime
    });
  }

  Future<List<SubComment>> getSubCommentsByMailCommentUid(
      String mainCommentUid) async {
    List<SubComment> subComments = [];

    await db
        .collection(subCommentsString)
        .where('mainCommentUid', isEqualTo: mainCommentUid)
        .get()
        .then((QuerySnapshot querySnapshot) async {
      querySnapshot.docs.forEach((document) async {
        Timestamp postedTimestamp = document.data()['dateTime'];
        subComments.add(SubComment(
          uid: document.id,
          mainCommentUid: document.data()['mainCommentUid'],
          user: await this.getUserDetailByUserUid(document.data()['userUid']),
          commentText: document.data()['commentText'],
          dateTime: DateTime.fromMillisecondsSinceEpoch(
              postedTimestamp.seconds * 1000),
        ));
      });
    });

    return subComments;
  }
}
